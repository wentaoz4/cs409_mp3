const express = require('express');
const router = express.Router();
const Task = require('../models/task');  
const User = require('../models/user');
// GET: List of tasks with query functionality
router.get('/', async (req, res) => {
    try {
        let query = Task.find();

        if (req.query.where) {
            query.where(JSON.parse(req.query.where));
        }

        if (req.query.sort) {
            query.sort(JSON.parse(req.query.sort));
        }

        if (req.query.select) {
            query.select(JSON.parse(req.query.select));
        }

        if (req.query.skip) {
            query.skip(parseInt(req.query.skip, 10));
        }

        if (req.query.limit) {
            query.limit(Math.min(100, parseInt(req.query.limit, 10)));
        }

        if (req.query.count && req.query.count === 'true') {
            const count = await Task.countDocuments(query);
            return res.json({ "message": "OK", "data": { count: count } });
        } else {
            const tasks = await query.exec();
            if (tasks.length === 0) {
                return res.status(404).json({ "message": "No tasks found", "data": {} });
            }
            res.json({ "message": "OK", "data": tasks });
        }
    } catch (err) {
        res.status(500).json({ "message": "Error fetching tasks", "data": {} });
    }
});

// POST: Create a new task
router.post('/', async (req, res) => {
    // const task = new Task(req.body);
    // try {
    //     const newTask = await task.save();
    //     res.status(201).json({ "message": "Task created successfully", "data": newTask });
    // } catch (err) {
    //     res.status(400).json({ "message": "Error creating task", "data": {} });
    // }
   

    // Create a new task with the provided data
    const task = new Task(req.body);

    try {
        // Save the new task
        const newTask = await task.save();

        // If an assignedUser is provided, update the user's pendingTasks
        
        if (task.assignedUser) {
            // Ensuring assignedUser is a valid ObjectID
            const userId = task.assignedUser; 
            try {
                
                await User.findByIdAndUpdate(userId, {
                    $push: { pendingTasks: newTask._id }
                });
                // return res.status(400).json({ "message": "ssss", "data": {} });
            } catch (updateError) {
                console.error("Error updating user:", updateError);
                return res.status(500).json({ "message": "Error updating user", "data": {} });
            }
        }

        res.status(201).json({ "message": "Task created successfully", "data": newTask });
    } catch (err) {
        res.status(400).json({ "message": "Error creating task", "data": {} });
    }
});

// GET: Specific task by ID 
router.get('/:id', async (req, res) => {
    try {
         
        let query = Task.findById(req.params.id);
 
        if (req.query.select) {
            const selectQuery = JSON.parse(req.query.select);
            query.select(selectQuery);
        }

        const task = await query.exec();

        if (!task) {
            return res.status(404).json({ "message": "Task not found", "data": {} });
        }

        res.json({ "message": "Task retrieved successfully", "data": task });
    } catch (err) {
        res.status(500).json({ "message": "Error fetching task", "data": {}, "error": err.message });
    }
});


// PUT: Update a specific task
router.put('/:id', async (req, res) => {
    try {
        const task = await Task.findById(req.params.id);
        if (!task) {
            return res.status(404).json({ "message": "Task not found", "data": {} });
        }

         
        if (req.body.assignedUser && task.assignedUser !== req.body.assignedUser) {
            
            if (task.assignedUser) {
                await User.findByIdAndUpdate(task.assignedUser, {
                    $pull: { pendingTasks: task._id }
                });
            }
 
            await User.findByIdAndUpdate(req.body.assignedUser, {
                $addToSet: { pendingTasks: task._id }
            });
        }
 
        Object.assign(task, req.body);
 
        const updatedTask = await task.save();

        res.json({ "message": "Task updated successfully", "data": updatedTask });
    } catch (err) {
        res.status(400).json({ "message": "Error updating task", "data": {} });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const taskId = req.params.id;
        const task = await Task.findById(req.params.id);
        if (!task) {
            return res.status(404).json({ "message": "Task not found", "data": {} });
        }

   
        if (task.assignedUser) {
            // Ensuring assignedUser is a valid ObjectID
            const userId = task.assignedUser; 
             
            
            try {
                
                await User.findByIdAndUpdate(userId, {
                    $pull: { pendingTasks: taskId }
                });
                // return res.status(400).json({ "message": "ssss", "data": {} });
            } catch (updateError) {
                console.error("Error updating user:", updateError);
                return res.status(500).json({ "message": "Error updating user", "data": {} });
            }
        }

        await task.remove();

        res.json({ "message": "Task deleted successfully", "data": {} });
    } catch (err) {
        res.status(500).json({ "message": "Error deleting task", "data": {} });
    }
});

module.exports = router;



 