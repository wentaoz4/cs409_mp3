const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Task = require('../models/task');

 
router.get('/', async (req, res) => {
    try {
 
        let query = User.find();

        if (req.query.where) {
            const whereQuery = JSON.parse(req.query.where);
            query.where(whereQuery);
        }
        if (req.query.sort) {
            const sortQuery = JSON.parse(req.query.sort);
            query.sort(sortQuery);
        }
        if (req.query.select) {
            const selectQuery = JSON.parse(req.query.select);
            query.select(selectQuery);
        }

        if (req.query.skip) {
            query.skip(Number(req.query.skip));
        }

      
        // const limit = req.query.limit ? Number(req.query.limit) : 0; // 0 means no limit in mongoose
        // query.limit(limit);

        // if (req.query.count && req.query.count === 'true') {
        //     const count = await query.countDocuments();
        //     return res.json({ "message": "OK", "data": { count: count } });
        // }
 
        // const users = await query.exec();
        // if (users.length === 0) {
        //     return res.status(404).json({ "message": "User not found", "data": {} });
        // }

        
        // res.json({ "message": "OK", "data": users });


        if (req.query.limit) {
            query.limit(Math.min(100, parseInt(req.query.limit, 10)));
        }

        if (req.query.count && req.query.count === 'true') {
            const count = await User.countDocuments(query);
            return res.json({ "message": "OK", "data":  count  });
        } else {
            const users = await query.exec();
            if (users.length === 0) {
                return res.status(404).json({ "message": "No users found", "data": {} });
            }
            res.json({ "message": "OK", "data": users });
        }
    } catch (err) {
        res.status(500).json({ "message": "Error fetching users", "data": {}, "error": err.message });
    }
});


router.post('/', async (req, res) => {
    console.log(req.body);
    const user = new User(req.body);
    try {
        const newUser = await user.save();
        if (user.pendingTasks && user.pendingTasks.length > 0) {
            const taskIds = user.pendingTasks; 
            try {
                await Task.updateMany(
                    { _id: { $in: taskIds } },
                    { $set: { assignedUser: newUser._id } }
                );
            } catch (updateError) {
                console.error("Error updating task:", updateError);
                return res.status(500).json({ "message": "Error updating task", "data": {} });
            }
        }
        res.status(201).json({ "message": "User created successfully", "data": newUser });
    } catch (err) {
        let errorMessage = "Error creating user";
        if (err.name === 'ValidationError') {
            errorMessage += ": " + err.message; 
        }
        res.status(400).json({ "message": errorMessage, "data": {} });
    }
});
 
router.get('/:id', async (req, res) => {
    try {
        let query = User.findById(req.params.id);

        if (req.query.select) {
            const selectQuery = JSON.parse(req.query.select);
            query.select(selectQuery);
        }

        const user = await query.exec();

        if (!user) {
            return res.status(404).json({ "message": "User not found", "data": {} });
        }

        res.json({ "message": "OK", "data": user });
    } catch (err) {
        res.status(500).json({ "message": "Error fetching user", "data": {}, "error": err.message });
    }
});

router.put('/:id', async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            return res.status(404).json({ "message": "User not found", "data": {} });
        }

        Object.assign(user, req.body);

        const updatedUser = await user.save();

        if (req.body.pendingTasks) {
            const tasksToUnassign = user.pendingTasks.filter(t => !req.body.pendingTasks.includes(t.toString()));

            // Unassign tasks that are no longer in the user's pendingTasks
            if (tasksToUnassign.length > 0) {
                await Task.updateMany(
                    { _id: { $in: tasksToUnassign } },
                    { $set: { assignedUser: '', assignedUserName: 'unassigned' } }
                );
            }

            // Assign new tasks to the user
            await Task.updateMany(
                { _id: { $in: req.body.pendingTasks.map(id => mongoose.Types.ObjectId(id)) } },
                { $set: { assignedUser: userId, assignedUserName: user.name } }
            );
        }

        res.json({ "message": "User updated successfully", "data": updatedUser });
    } catch (err) {
        res.status(400).json({ "message": "Error updating user", "data": {} });
    }
});
router.delete('/:id', async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            return res.status(404).json({ "message": "User not found", "data": {} });
        }
        await user.remove();


        await Task.updateMany(
            { _id: { $in: user.pendingTasks } },
            { $set: { assignedUser: '', assignedUserName: 'unassigned' } }
        );

        res.json({ "message": "User deleted successfully", "data": {} });
    } catch (err) {
        res.status(500).json({ "message": "An unexpected server error occurred", "data": {} });
    }
});
module.exports = router;




 